CREATE DATABASE [MiniDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'MiniDB', FILENAME = N'C:\Users\Dreamer\MiniDB.mdf' , SIZE = 8192KB , FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'MiniDB_log', FILENAME = N'C:\Users\Dreamer\MiniDB_log.ldf' , SIZE = 8192KB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [MiniDB] SET COMPATIBILITY_LEVEL = 130
GO
ALTER DATABASE [MiniDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [MiniDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MiniDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MiniDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MiniDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [MiniDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [MiniDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [MiniDB] SET AUTO_CREATE_STATISTICS ON(INCREMENTAL = OFF)
GO
ALTER DATABASE [MiniDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MiniDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MiniDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MiniDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MiniDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MiniDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MiniDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MiniDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [MiniDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MiniDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MiniDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MiniDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [MiniDB] SET  READ_WRITE 
GO
ALTER DATABASE [MiniDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [MiniDB] SET  MULTI_USER 
GO
ALTER DATABASE [MiniDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [MiniDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [MiniDB] SET DELAYED_DURABILITY = DISABLED 
GO
USE [MiniDB]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = Off;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = Primary;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = On;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = Primary;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = Off;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = Primary;
GO
USE [MiniDB]
GO
IF NOT EXISTS (SELECT name FROM sys.filegroups WHERE is_default=1 AND name = N'PRIMARY') ALTER DATABASE [MiniDB] MODIFY FILEGROUP [PRIMARY] DEFAULT
GO
