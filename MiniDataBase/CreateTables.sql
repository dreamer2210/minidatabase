USE MiniDB

CREATE TABLE [dbo].[Location]
(
	[Id] int PRIMARY KEY IDENTITY(1, 1) NOT NULL,
	[Country] nvarchar(50) NOT NULL,
	[City] nvarchar(50) NOT NULL
)

CREATE TABLE [dbo].[Passport]
(
	[Id] int PRIMARY KEY IDENTITY(1, 1) NOT NULL,
	[WorkerId] int NOT NULL,
	[Series] int NOT NULL,
	[Number] int NOT NULL,
	[BirthDate] date NOT NULL,
)

CREATE TABLE [dbo].[Worker] 
(
	[Id] int PRIMARY KEY IDENTITY(1, 1) NOT NULL,
	[FirstName] nvarchar(50) NOT NULL,
	[LastName] nvarchar(50) NOT NULL,
	[Position] nvarchar(100) NOT NULL,
	[Experience] int NOT NULL,
	[Salary] money NOT NULL,
	[PassportId] int NOT NULL FOREIGN KEY REFERENCES Passport(Id),
	[LocationId] int NOT NULL FOREIGN KEY REFERENCES Location(Id),
)

ALTER TABLE Passport ADD CONSTRAINT ID FOREIGN KEY ([WorkerId]) REFERENCES Worker(Id)
